# Stand server

AI Lab stand server that interacts with dongle

## Docker

## Build docker

You can build docker image using Dockerfile

`docker image build -t dongle-server .`

### Launch docker

To launch docker image of flask server on port 5000

`docker run -p 5000:5000 -d dongle-server`

## API

TODO: Document API
