# start by pulling the python image
FROM continuumio/miniconda3

# EXPOSE 5003

RUN apt-get update
RUN apt-get install python3-opencv -y

# copy the requirements file into the image
COPY ./environment.yml /app/environment.yml

WORKDIR /app

RUN echo "Creating env"
RUN conda env create -f environment.yml

# Make RUN commands use the new environment:
SHELL ["conda", "run", "-n", "ServerMTS-env", "/bin/bash", "-c"]

RUN pip install --upgrade pip
RUN python -m pip install --upgrade pip

# copy every content from the local file to the image
COPY ./app /app

# RUN pip install opencv-python

RUN echo "Make sure flask is installed:"
RUN python -c "import flask"

RUN echo "Make sure opencv is installed:"
RUN python -c "import cv2"

RUN echo "Install kneron-plus"
COPY ./kp /kp
RUN pip install /kp/python/package/ubuntu/KneronPLUS-1.3.0-py3-none-any.whl

RUN rm -r /kp

RUN echo "Make sure kp is installed:"
RUN python -c "import kp"

# configure the container to run in an executed manner
ENTRYPOINT ["conda", "run", "--no-capture-output", "-n", "ServerMTS-env", "python", "main.py"]