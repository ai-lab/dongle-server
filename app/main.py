# Launch API with prefix

from werkzeug.middleware.dispatcher import DispatcherMiddleware
from werkzeug.wrappers import Response

import os
os.environ['PYTHON_NPU_SERVER_CONFIG'] = 'python_npu_server.cfg'

print('env var setted')

import waitress

from api_flask import get_app, get_info

app = get_app()
info = get_info()

prefix = f'/{info["name"]}/{info["version"]}';


app.wsgi_app = DispatcherMiddleware(
    Response('Not Found1', status=404),
    {prefix: app.wsgi_app}
)

waitress.serve(app, listen="0.0.0.0:5000")