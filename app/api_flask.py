# from crypt import methods
import io

from flask import Flask, request
from flask import make_response, abort, jsonify
import os
from os import path
from os import listdir, walk

from base64 import encodebytes
from flask.helpers import send_file

import jsonlines as jl

from PIL import Image
import cv2

import kp

from utils.ExampleHelper import get_device_usb_speed_by_port_id
from utils.ExamplePostProcess import post_process_tiny_yolo_v3

from flask_settings import default_settings

# import kp #kneron plus

if __name__ == '__main__':
    os.environ['PYTHON_NPU_SERVER_CONFIG'] = 'python_npu_server.cfg'

# some state data

firmware_list = []
current_firmware = None

usb_port_id = 0

device_group = None
model_nef_descriptor = None
generic_raw_image_header = None

# API

app = Flask(__name__)

app.config.from_object(default_settings)

app.config.from_envvar('PYTHON_NPU_SERVER_CONFIG')

# import firmwares
with jl.open(app.config["FIRMWARE_LIST_FILE"]) as input:
    for firmware in input:
        firmware_list.append(firmware)

# print(app.config)

@app.route('/dongle/connect', methods=['GET'])
def dongle_connect():
    """
    check device USB speed (Recommend run KL520 at high speed)
    """
    try:
        if kp.UsbSpeed.KP_USB_SPEED_HIGH != get_device_usb_speed_by_port_id(usb_port_id=usb_port_id):
            print('\033[91m' + '[Warning] Device is not run at high speed.' + '\033[0m')
    except Exception as exception:
        print('Error: check device USB speed fail, port ID = \'{}\', error msg: [{}]'.format(usb_port_id,
                                                                                             str(exception)))
        exit(0)

    """
    connect the device
    """
    global device_group
    try:
        print('[Connect Device]')
        device_group = kp.core.connect_devices(usb_port_ids=[usb_port_id])
        print(' - Success')
    except kp.ApiKPException as exception:
        print('Error: connect device fail, port ID = \'{}\', error msg: [{}]'.format(usb_port_id,
                                                                                     str(exception)))
        exit(0)

@app.route('/firmwares', methods=['GET'])
def show_firmwares():
    # TODO: decide how to store firmwares
    # Now firmaware info stores in file firmwares.jl
    return {'firmwares': [f["name"] for f in firmware_list]}

@app.route('/firmwares/current', methods=['GET'])
def get_current_firmware():
    return {'current_firmware': current_firmware}

@app.route('/firmwares/current', methods=['POST'])
def set_current_firmware():
    if not request.json or not 'firmware' in request.json:
        abort(400)
    try:
        res = set_firmware(request.json['firmware'])
        return {'loaded_firmware':res}
    except Exception as err:
        abort(400, err)
        
@app.route('/images', methods=['GET'])
def get_images():
    image_names = []
    path_to_images = app.config['INPUT_IMAGES_DIR']
    image_names.extend([path.join(path_to_images, f) for f in listdir(path_to_images) if path.isfile(path.join(path_to_images, f))])
    path_to_images = app.config['OUTPUT_IMAGES_DIR']
    image_names.extend([path.join(path_to_images, f) for f in listdir(path_to_images) if path.isfile(path.join(path_to_images, f))])
    
    # images = []
    
    # for image in image_names:
    #     pil_img = Image.open(image, mode='r')
    #     byte_arr = io.BytesIO()
    #     pil_img.save(byte_arr, format='PNG') # convert the PIL image to byte array
    #     encoded_img = encodebytes(byte_arr.getvalue()).decode('ascii') # encode as base64
    #     images.append(encoded_img)
    
    return {'image_names' : image_names}
        
@app.route('/images/<name>', methods=['GET'])
def get_image(name):
    image_names = []
    path_to_images = app.config['INPUT_IMAGES_DIR']
    image_names.extend([path.join(path_to_images, f) for f in listdir(path_to_images) if f == name and path.isfile(path.join(path_to_images, f))])
    path_to_images = app.config['OUTPUT_IMAGES_DIR']
    image_names.extend([path.join(path_to_images, f) for f in listdir(path_to_images) if f == name and path.isfile(path.join(path_to_images, f))])
    
    a = send_file(image_names[0], mimetype='image/jpeg', as_attachment=True)
    return a
    
@app.route('/images', methods=['POST'])
def post_images():
    # TODO: name and image must align with restrictions
    if not app.config['INPUT_IMAGES_DIR']:
        raise ValueError('Uploads are disabled.')
    uploaded_files = request.files.getlist("")
    for file in uploaded_files:
        print(file)
        file.save(path.join(app.config['INPUT_IMAGES_DIR'], file.filename))
        
    return {"result":'Success'}

@app.route('/images', methods=['DELETE'])
def delete_images():
    images = []
    path_to_images = app.config['INPUT_IMAGES_DIR']
    images.extend([path.join(path_to_images, f) for f in listdir(path_to_images) if path.isfile(path.join(path_to_images, f))])
    path_to_images = app.config['OUTPUT_IMAGES_DIR']
    images.extend([path.join(path_to_images, f) for f in listdir(path_to_images) if path.isfile(path.join(path_to_images, f))])
    for file in images:
        os.remove(file)
        
    return {'deleted' : len(images),
            'files' : images}
    
@app.route('/images/<name>', methods=['DELETE'])
def delete_image(name):
    images = []
    path_to_images = app.config['INPUT_IMAGES_DIR']
    images.extend([path.join(path_to_images, f) for f in listdir(path_to_images) if path.isfile(path.join(path_to_images, f)) and f == name])
    path_to_images = app.config['OUTPUT_IMAGES_DIR']
    images.extend([path.join(path_to_images, f) for f in listdir(path_to_images) if path.isfile(path.join(path_to_images, f)) and f == name])
    for file in images:
        os.remove(file)
        
    return {'deleted' : len(images),
            'files' : images}
    
@app.route('/recognition', methods=['POST'])
def recognize_images():
    images = []
    path_to_images = app.config['INPUT_IMAGES_DIR']
    images.extend([path.join(path_to_images, f) for f in listdir(path_to_images) if path.isfile(path.join(path_to_images, f))])
    path_to_images = app.config['OUTPUT_IMAGES_DIR']
    
    # TODO: Send images to NPU and return results
    for image in images:
        img = cv2.imread(filename=image)
        img_bgr565 = cv2.cvtColor(src=img, code=cv2.COLOR_BGR2BGR565)
        
        try:
            kp.inference.generic_raw_inference_send(device_group=device_group,
                                                    generic_raw_image_header=generic_raw_image_header,
                                                    image=img_bgr565,
                                                    image_format=kp.ImageFormat.KP_IMAGE_FORMAT_RGB565)

            generic_raw_result = kp.inference.generic_raw_inference_receive(device_group=device_group,
                                                                            generic_raw_image_header=generic_raw_image_header,
                                                                            model_nef_descriptor=model_nef_descriptor)
        except kp.ApiKPException as exception:
            print(' - Error: inference failed, error = {}'.format(exception))
            exit(0)
    
        box_list = postprocess(generic_raw_result)
        
        new_image = print_boxes(img.copy(), box_list)
        
        cv2.imwrite(path_to_images+image, new_image)
        
    return {'recognized' : len(images),
            'files' : images}
    

@app.errorhandler(400)
def not_found(error):
    return jsonify(error=str(error)), 400

@app.errorhandler(404)
def not_found(error):
    return jsonify({'error': 'Not found'}), 404

# info

def get_info():
    return {
        'name': app.config['NAME'],
        'version': app.config['VERSION']
    }
    
# functions

def postprocess(generic_raw_result):
    global generic_raw_image_header
    
    print('[Retrieve Inference Node Output ]')
    
    inf_node_output_list = []
    for node_idx in range(generic_raw_result.header.num_output_node):
        inference_float_node_output = kp.inference.generic_inference_retrieve_float_node(node_idx=node_idx,
                                                                                            generic_raw_result=generic_raw_result,
                                                                                            channels_ordering=kp.ChannelOrdering.KP_CHANNEL_ORDERING_CHW)
        inf_node_output_list.append(inference_float_node_output)
    
    # do post-process
    _yolo_result = post_process_tiny_yolo_v3(inference_float_node_output_list=inf_node_output_list,
                                                image_width=generic_raw_image_header.width,
                                                image_height=generic_raw_image_header.height,
                                                thresh_value=0.2)

    return _yolo_result.box_list

def print_boxes(image, box_list):
    # draw bounding box
    for yolo_result in box_list:
        b = 100 + (25 * yolo_result.class_num) % 156
        g = 100 + (80 + 40 * yolo_result.class_num) % 156
        r = 100 + (120 + 60 * yolo_result.class_num) % 156
        color = (b, g, r)

        cv2.rectangle(img=image,
                        pt1=(int(yolo_result.x1), int(yolo_result.y1)),
                        pt2=(int(yolo_result.x2), int(yolo_result.y2)),
                        color=color,
                        thickness=3)
        
    return image

def set_firmware(firmware):
    global model_nef_descriptor
    global generic_raw_image_header
    
    firmware = [f for f in firmware_list if f['name']==firmware]
    if len(firmware) == 0:
        raise Exception("Invalid firmware name")
    if len(firmware) > 1:
        raise Exception("Found more than one firmware")
    firmware = firmware[0]
    global current_firmware
    current_firmware = firmware
    # TODO: test this code IRL
    # """
    # upload firmware to device
    # """
    try:
        print('[Upload Firmware]')
        kp.core.load_firmware_from_file(device_group=device_group,
                                        scpu_fw_path=current_firmware['scpu'],
                                        ncpu_fw_path=current_firmware['ncpu'])
        print(' - Success')
    except kp.ApiKPException as exception:
        print('Error: upload firmware failed, error = \'{}\''.format(str(exception)))
        exit(0)

    # """
    # upload model to device
    # """
    try:
        print('[Upload Model]')
        model_nef_descriptor = kp.core.load_model_from_file(device_group=device_group,
                                                            file_path=current_firmware['model'])
        print(' - Success')
    except kp.ApiKPException as exception:
        print('Error: upload model failed, error = \'{}\''.format(str(exception)))
        exit(0)
        
    """
    prepare app generic inference config
    """
    generic_raw_image_header = kp.GenericRawImageHeader(
        model_id=model_nef_descriptor.models[0].id,
        resize_mode=kp.ResizeMode.KP_RESIZE_ENABLE,
        padding_mode=kp.PaddingMode.KP_PADDING_CORNER,
        normalize_mode=kp.NormalizeMode.KP_NORMALIZE_KNERON,
        inference_number=0
    )
    
    return current_firmware

def get_app():
    return app

if __name__ == '__main__':
    # os.environ['PYTHON_NPU_SERVER_CONFIG'] = 'H:\Programs\MTS-Kneron\Server\python_npu_server.cfg'
    app.run(debug=True)
    # app.run()
    # waitress.serve(app, listen="0.0.0.0:5001")
    