class default_settings:
    
    NAME = 'api'
    VERSION = 'v1'

    INPUT_IMAGES_DIR = "images/input11/"
    OUTPUT_IMAGES_DIR = "images/output11/"
    FIRMWARE_LIST_FILE = "firmwares.jl"

    PREFIX = f'/{NAME}/{VERSION}'
    